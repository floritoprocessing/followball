class Ball {

  PVector targetPos;

  PVector position;
  PVector lastPos;

  PVector movement;


  // 2.5 0.1
  // 0.5 0.1
  // 0.5 0.5
  float acceleration = 2.5;
  float damping = 0.1; //0.95

  Ball() {
    targetPos = new PVector();
    position = new PVector(random(400), random(400), random(400));
    lastPos = position.copy();
    movement = PVector.random3D();
  }

  void setTarget(float tx, float ty, float tz) {
    targetPos.set(tx, ty, tz);
  }
  void setTarget(PVector t) {
    targetPos.set(t);
  }

  void tick() {
    lastPos.set(position);
    position.add(movement);

    PVector distanceToTarget = PVector.sub(targetPos, position);
    movement.add( PVector.mult(distanceToTarget, acceleration) );
    //xm = xm + (targetX-x)*acceleration;
    //ym = ym + (targetY-y)*acceleration;

    movement.mult(damping);
    //xm = xm * damping;
    //ym = ym * damping;
  }

  void draw(boolean drawing) {
    ellipseMode(CENTER);

    noFill();
    pushMatrix();
    translate(position.x, position.y, position.z);
    float r;
    if (drawing) {
      stroke(255, 80);
      r=20;
    } else {
      stroke(255);
      r=4;
    }
    rotateY(radians(-rotYDeg));
    ellipse(0, 0, r, r);
    rotateY(HALF_PI);
    ellipse(0, 0, r, r);
    rotateX(HALF_PI);
    ellipse(0, 0, r, r);
    popMatrix();
  }
}
