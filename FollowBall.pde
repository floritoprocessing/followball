// Drag mouse to draw.
// [a] and [s] controls y rotation
// update: Ball is now drawn as a pseudo-sphere (one circles per plane). Y-rotation control with keyboard
//TODO: potential GPU optimisation: turn a collection of lines into a PShape to keep them on the GPU. (less vertex updating)

Ball[] balls;
ArrayList<MultiLine> allLines = new ArrayList<MultiLine>();
ArrayList<MultiLine> drawLines = new ArrayList<MultiLine>();
float rotYDeg, rotYDecInc;

void setup() {
  size(700, 700, P3D);

 rotYDecInc = 1;
  balls = new Ball[30];
  for (int i=0; i<balls.length; i++) 
    balls[i] = new Ball();
    
  hint(DISABLE_DEPTH_TEST);
}

void keyReleased() {
  if (key=='a') rotYDecInc-=0.2;
  if (key=='s') rotYDecInc+=0.2;
}

void mousePressed() {
  println("Adding new MultiLines to DrawLines");
  drawLines.clear();
  for (int i=0; i<balls.length; i++) {
    drawLines.add( new MultiLine() );
  }
}

void mouseReleased() {
  allLines.addAll(drawLines);
  drawLines.clear();
  println("allLines.size()="+allLines.size());
}

void draw() {
  background(0);

  tickBalls();

  if (mousePressed) {
    addPointsToDrawLines();
  }

  translate(width/2,height/2);
  
  rotateY(radians(rotYDeg));
  
  drawBalls();
  drawMultiLines();
  
  
  
  
  rotYDeg += rotYDecInc;
}

void tickBalls() {
  
  // center around 0/0
  PVector inputPos = new PVector(mouseX-width/2, mouseY-height/2, 0);
  
  PMatrix3D rot = new PMatrix3D();
  rot.rotateY(-radians(rotYDeg));
  PVector out = new PVector();
  rot.mult(inputPos, out);
  inputPos = out;
  
  for (int i=0; i<balls.length; i++) {
    if (i==0) {
      balls[i].setTarget(inputPos);
    }
    else
      balls[i].setTarget(balls[i-1].position);
  }

  for (int i=0; i<balls.length; i++) {
    balls[i].tick();
  }
}

void addPointsToDrawLines() {
  for (int i=0; i<balls.length; i++) {
    drawLines.get(i).addPoint( balls[i].position );
  }
}

void drawMultiLines() {

  blendMode(ADD);
  stroke(255, 40, 12, 64);
  for (MultiLine l : drawLines) {
    l.drawOn(this);
  }
  for (MultiLine l : allLines) {
    l.drawOn(this);
  }
}

void drawBalls() {
  for (int i=0; i<balls.length; i++) {
    balls[i].draw(mousePressed);
  }
}
