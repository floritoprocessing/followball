class MultiLine {

  ArrayList<PVector> points = new ArrayList<PVector>();

  void addPoint(PVector p) {
    points.add(p.copy());
  }

  void drawOn(PGraphics pg) {
    pg.beginShape(LINE_STRIP);
    for (PVector p : points) {
      pg.vertex(p.x, p.y, p.z);
    }
    pg.endShape();
  }

  void drawOn(PApplet pa) {
    pa.beginShape(LINE_STRIP);
    for (PVector p : points) {
      pa.vertex(p.x, p.y, p.z);
    }
    pa.endShape();
  }
}
